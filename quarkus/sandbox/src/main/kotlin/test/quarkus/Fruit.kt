package test.quarkus

data class Fruit(val name: String, val description: String)
