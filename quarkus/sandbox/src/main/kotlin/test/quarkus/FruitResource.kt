package test.quarkus

import javax.enterprise.context.RequestScoped
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON

@Path("/fruit")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@RequestScoped
class FruitResource {

    private final val fruits: MutableSet<Fruit> = mutableSetOf()

    init {
        fruits.add(Fruit("orange", "winter fruit"))
        fruits.add(Fruit("pinapple", "tropical fruit"))
    }

    @GET
    fun list(): Set<Fruit> = fruits

    @POST
    fun add(fruit: Fruit) {
        fruits.add(fruit)
    }

    @DELETE
    fun delete(fruit: Fruit) {
        fruits.removeIf { it.name == fruit.name }
    }

    @DELETE
    @Path("/all")
    fun deleteAll() {
        fruits.clear()
    }
}