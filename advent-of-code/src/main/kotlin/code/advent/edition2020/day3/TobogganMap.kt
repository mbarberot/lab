package code.advent.edition2020.day3

class TobogganMap(val template: List<String>) {

    fun get(position: Position): Char {

        return getRow(position).elementAt(position.column)
    }

    private fun getRow(position: Position): String {
        val rowTemplate = template.get(position.row)

        return extendRow(rowTemplate, position.column)
    }

    private fun extendRow(rowTemplate: String, columnIndex: Int): String {
        var row = rowTemplate

        while (columnIndex >= row.length) {
            row += rowTemplate
        }

        return row
    }

    fun bottomReached(position: Position): Boolean {
        return position.row >= template.size - 1
    }
}