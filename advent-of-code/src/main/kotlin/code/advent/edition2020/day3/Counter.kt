package code.advent.edition2020.day3

interface Counter {
    fun handle(symbol: Char)
    fun getValue(): Int
}


