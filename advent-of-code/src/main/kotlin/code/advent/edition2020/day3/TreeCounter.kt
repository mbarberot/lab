package code.advent.edition2020.day3

class TreeCounter : Counter {
    private var trees = 0

    override fun handle(symbol: Char) {
        if (symbol == '#') {
            trees++
        }
    }

    override fun getValue() = trees
}
