package code.advent.edition2020.day3

import code.advent.utils.readFile

object Day3

fun mainV1() {
    val treeCounter = TreeCounter()
    TravelByToboggan(readFile("input.txt", of = Day3)).travel(treeCounter)
    println(treeCounter.getValue())
}

fun main() {
    val trees = listOf(
        travelAndCountTrees { row, col -> Pair(row + 1, col + 1) },
        travelAndCountTrees { row, col -> Pair(row + 1, col + 3) },
        travelAndCountTrees { row, col -> Pair(row + 1, col + 5) },
        travelAndCountTrees { row, col -> Pair(row + 1, col + 7) },
        travelAndCountTrees { row, col -> Pair(row + 2, col + 1) },
    )

    trees.forEach {
        println(it)
    }

    println(trees.map { it.toLong() }.reduce { acc, i -> acc * i })
}

private fun travelAndCountTrees(movement: (Int, Int) -> Pair<Int, Int>): Int {
    val treeCounter = TreeCounter()
    TravelByToboggan(readFile("input.txt", of = Day3)).travel(treeCounter, movement)
    return treeCounter.getValue()
}
