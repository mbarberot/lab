package code.advent.edition2020.day3

class TravelByToboggan(input: List<String>) {
    private val map = TobogganMap(input)

    fun travel(counter: Counter, movement: (Int, Int) -> Pair<Int, Int> = { row, col -> Pair(row + 1, col + 3) }) {
        val position = Position(0, 0)

        do {
            position.move(movement)
            val char = map.get(position)
            counter.handle(char)

        } while (!map.bottomReached(position))
    }
}