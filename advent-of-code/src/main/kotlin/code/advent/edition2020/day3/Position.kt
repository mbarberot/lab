package code.advent.edition2020.day3

class Position(initialRowIndex: Int, initialColumnIndex: Int) {
    var row = initialRowIndex
    var column = initialColumnIndex

    fun move(movement: (Int, Int) -> Pair<Int, Int>): Pair<Int, Int> {
        val newPosition = movement.invoke(row, column)
        setPosition(newPosition)
        return newPosition
    }

    private fun setPosition(newPosition: Pair<Int, Int>) {
        row = newPosition.first
        column = newPosition.second
    }
}