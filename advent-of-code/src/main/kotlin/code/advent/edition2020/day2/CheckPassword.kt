package code.advent.edition2020.day2

import code.advent.edition2020.day2.rules.Rule
import code.advent.edition2020.day2.rules.RuleExtractor

class PasswordChecker(val ruleExtractor: RuleExtractor) {
    fun checkPassword(input: String): Result {
        val (password, rule) = readLine(input)
        return rule.checkPassword(password)
    }

    private fun readLine(input: String): Pair<String, Rule> {
        val parts = input.split(":")
        val password = extractPassword(parts[1])
        val rule = ruleExtractor.extractRule(parts[0])
        return Pair(password, rule)
    }

    private fun extractPassword(input: String) = input.trim()



}
