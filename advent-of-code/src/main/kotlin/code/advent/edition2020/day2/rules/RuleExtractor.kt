package code.advent.edition2020.day2.rules

sealed class RuleExtractor {
    fun extractRule(input: String): Rule {
        val parts = input.split(" ")
        val ints = parts[0]
        val char: Char = parts[1].first()

        val (a, b) = extractInts(ints)

        return createRule(char, a, b)
    }

    abstract fun createRule(char: Char, a: Int, b: Int): Rule

    private fun extractInts(input: String): Pair<Int, Int> {
        val parts = input.split("-")
        val min = parts[0].toInt()
        val max = parts[1].toInt()
        return Pair(min, max)
    }
}