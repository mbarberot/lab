package code.advent.edition2020.day2.rules

class RuleExtractorV1: RuleExtractor() {
    override fun createRule(char: Char, a: Int, b: Int): Rule = RuleV1(char, a..b)
}