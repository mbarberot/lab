package code.advent.edition2020.day2.rules

class RuleExtractorV2: RuleExtractor() {
    override fun createRule(char: Char, a: Int, b: Int): Rule = RuleV2(char, a, b)
}