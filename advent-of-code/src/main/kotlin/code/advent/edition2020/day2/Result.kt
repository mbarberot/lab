package code.advent.edition2020.day2

enum class Result {
    VALID,
    INVALID
}