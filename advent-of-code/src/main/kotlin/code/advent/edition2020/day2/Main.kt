package code.advent.edition2020.day2

import code.advent.edition2020.day2.rules.RuleExtractorV2
import code.advent.utils.readFile

fun main() {

//    val passwordChecker = PasswordChecker(RuleExtractorV1())
    val passwordChecker = PasswordChecker(RuleExtractorV2())
    val validPasswordCount =
        readFile("input.txt", of = Day2)
            .map { passwordChecker.checkPassword(it) }
            .filter { it == Result.VALID }
            .count()

    println("Valid passwords : $validPasswordCount")

}

object Day2