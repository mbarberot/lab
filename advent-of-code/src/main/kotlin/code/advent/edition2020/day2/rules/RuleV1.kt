package code.advent.edition2020.day2.rules

import code.advent.edition2020.day2.Result

class RuleV1(val expectedChar: Char, val expectedOccurences: IntRange) : Rule {
    override fun checkPassword(password: String) : Result {
        val count = password.count { it == expectedChar }

        return if(count in expectedOccurences)
            Result.VALID
        else
            Result.INVALID
    }
}