package code.advent.edition2020.day2.rules

import code.advent.edition2020.day2.Result

interface Rule {
    fun checkPassword(password: String): Result
}