package code.advent.edition2020.day2.rules

import code.advent.edition2020.day2.Result

class RuleV2(val char: Char, val min: Int, val max: Int) : Rule {
    override fun checkPassword(password: String): Result {

        val firstOccurenceValid = password.elementAt(min - 1) == char
        val secondOccurenceValid = password.elementAt(max - 1) == char

        return if (firstOccurenceValid && secondOccurenceValid)
            Result.INVALID
        else if (firstOccurenceValid || secondOccurenceValid)
            Result.VALID
        else
            Result.INVALID
    }
}