package code.advent.edition2020.day4

import code.advent.utils.readFile

object Day4

fun main() {
    val validator = PassportValidator()

    val (_, passports) = readFile("input.txt", of = Day4)
        .fold(Pair("", mutableListOf<String>())) { acc, s ->
            val (str, passports) = acc

            if (s.trim().isEmpty()) {
                passports.add(str)
                Pair("", passports)
            } else {
                Pair("$str $s", passports)
            }
        }

    println(passports.count { validator.validate(it) })
}

