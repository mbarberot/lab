package code.advent.utils

import java.io.File
import java.io.IOException

fun readFile(file: String, of: Any): List<String> {
    val resource = of.javaClass.getResource(file)
    if (resource != null)
        return File(resource.file).readLines()
    else
        throw IOException("Cannot read $file")
}

fun readInputFile(of: Any) = readFile("input.txt", of)