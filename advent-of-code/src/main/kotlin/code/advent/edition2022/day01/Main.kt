package code.advent.edition2022.day01

import code.advent.utils.readInputFile

fun main() {
    val input = readInputFile(Day01)
    val caloriesList = parseCaloriesList(input)

    println("Most calories on an elf : ${findMostCalories(caloriesList)}")
    println("Most calories on top 3 elfs : ${findTop3MostCalories(caloriesList)}")
}


object Day01