package code.advent.edition2022.day01

data class ElfCalories(val calories: List<Int>) {

    val total: Int
        get() = calories.sum()
}
