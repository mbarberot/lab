package code.advent.edition2022.day01

fun findMostCalories(caloriesList: List<ElfCalories>): Int =
    caloriesList.map { it.total }.maxOf { it }


fun findTop3MostCalories(elfCalories: List<ElfCalories>): Int =
    elfCalories.sortedByDescending { it.total }
        .subList(0, 3)
        .sumOf { it.total }

fun parseCaloriesList(elfCalories: List<String>): List<ElfCalories> {
    val caloriesByElf = mutableListOf<List<Int>>()
    var aggregator = mutableListOf<Int>()
    for (line in elfCalories) {
        if (line.isBlank()) {
            caloriesByElf.add(aggregator)
            aggregator = mutableListOf()
            continue
        }

        aggregator.add(line.toInt())
    }
    caloriesByElf.add(aggregator)
    return caloriesByElf.map { ElfCalories(it) }
}
