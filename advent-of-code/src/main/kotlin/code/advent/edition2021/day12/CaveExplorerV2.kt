package code.advent.edition2021.day12

class CaveExplorerV2 {

    companion object {
        fun explore(initialCave: Cave): CaveExplorerV2 {
            val caveExplorer = CaveExplorerV2()
            caveExplorer.explore(initialCave, Path(initialCave))
            return caveExplorer
        }
    }

    val pathsFound = PathCollector()

    private fun explore(cave: Cave, path: Path) {
        cave.visitConnectedCaves { nextCave ->

            if (nextCave.isEnd) {
                // end reached
                pathsFound.add(path.withNewStep(nextCave))
                return@visitConnectedCaves
            }

            if (!canVisit(nextCave, path)) {
                // dead end
                // you must only step back to a big cave
                return@visitConnectedCaves
            }

            explore(nextCave, path.withNewStep(nextCave))
        }
    }

    fun canVisit(cave: Cave, path: Path): Boolean {
        if(cave.isStart) {
            return false
        }

        if (cave.isBig) {
            return true
        }

        if (!path.alreadyVisited(cave)) {
            return true
        }

        if (path.canVisitTwice()) {
            return true
        }

        return false
    }
}