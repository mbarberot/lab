package code.advent.edition2021.day12

fun String.isUpperCase(): Boolean = this.all { it.isUpperCase() }