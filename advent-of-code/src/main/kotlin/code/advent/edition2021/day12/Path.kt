package code.advent.edition2021.day12

class Path(
    val caves: List<Cave> = listOf()
) {

    constructor(cave: Cave) : this(listOf(cave))

    fun withNewStep(cave: Cave): Path {
        val newPath = mutableListOf<Cave>()
        newPath.addAll(caves)
        newPath.add(cave)

        return Path(newPath)
    }

    fun alreadyVisited(cave: Cave) = caves.any { it.name == cave.name }

    fun canVisitTwice() =
        caves
            .filter { !it.isBig }
            .none { visitedTwice(it) }

    private fun visitedTwice(cave: Cave) = caves.count { it.name == cave.name } >= 2

    override fun toString() = caves.joinToString(",") { it.name }

}