package code.advent.edition2021.day12

data class Cave(
    val name: String,
) {
    val isBig = name.isUpperCase()
    val isStart = name == "start"
    val isEnd = name == "end"

    private val mutableConnectedCaves = mutableSetOf<Cave>()
    private val connectedCaves = mutableConnectedCaves

    fun addConnectedCave(cave: Cave) {
        mutableConnectedCaves.add(cave)
    }

    fun visitConnectedCaves(block: (cave: Cave) -> Unit) {
        connectedCaves.forEach(block)
    }
}