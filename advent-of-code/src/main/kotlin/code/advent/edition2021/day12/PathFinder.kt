package code.advent.edition2021.day12

class PathFinder(
    val caveDescription: List<String>,
    val version: Int = 1
) {

    fun findPaths(): PathCollector {

        val map = buildCaveMap()

        val startingCave = map["start"] ?: throw Error("Must have a starting cave")

        return exploreCaves(startingCave)
    }

    private fun buildCaveMap(): Map<String, Cave> {
        val map = mutableMapOf<String, Cave>()

        // build map
        caveDescription.forEach { connexion: String ->
            val (start, end) = connexion.split("-")

            val startCave = map.getOrPut(start) { Cave(start) }
            val endCave = map.getOrPut(end) { Cave(end) }

            startCave.addConnectedCave(endCave)
            endCave.addConnectedCave(startCave)
        }
        return map
    }

    private fun exploreCaves(startingCave: Cave): PathCollector {
        return if (version == 1) {
            CaveExplorerV1.explore(startingCave).pathsFound
        } else {
            CaveExplorerV2.explore(startingCave).pathsFound
        }
    }
}

