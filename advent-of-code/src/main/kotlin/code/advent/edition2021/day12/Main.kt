package code.advent.edition2021.day12

import code.advent.utils.readInputFile

fun main() {
    val input = readInputFile(Day12)

    val pathsPartOne = PathFinder(input, version = 1).findPaths()
    val pathsPartTwo = PathFinder(input, version = 2).findPaths()

    println("Day 12")
    println("Paths count (part one) : ${pathsPartOne.size()}")
    println("Paths count (part two) : ${pathsPartTwo.size()}")
}

object Day12