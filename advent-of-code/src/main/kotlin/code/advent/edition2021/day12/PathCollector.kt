package code.advent.edition2021.day12

class PathCollector {
    private val paths = mutableSetOf<Path>()

    fun add(path: Path) {
        paths.add(path)
    }

    fun size() = paths.size

    fun toStringList() = paths.map { it.toString() }
}