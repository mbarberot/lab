package code.advent.edition2021.day12

class CaveExplorerV1 {

    companion object {
        fun explore(initialCave: Cave): CaveExplorerV1 {
            val caveExplorer = CaveExplorerV1()
            caveExplorer.explore(initialCave, Path(initialCave))
            return caveExplorer
        }
    }

    val pathsFound = PathCollector()

    private fun explore(cave: Cave, path: Path) {
        cave.visitConnectedCaves { nextCave ->

            if (nextCave.isEnd) {
                // end reached
                pathsFound.add(path.withNewStep(nextCave))
                return@visitConnectedCaves
            }

            if (path.alreadyVisited(nextCave) && !nextCave.isBig) {
                // dead end
                // you must only step back to a big cave
                return@visitConnectedCaves
            }

            explore(nextCave, path.withNewStep(nextCave))
        }
    }
}