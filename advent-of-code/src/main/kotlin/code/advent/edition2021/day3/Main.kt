package code.advent.edition2021.day3

import code.advent.edition2021.day3.diagnostic.Diagnostic
import code.advent.utils.readInputFile

fun main() {
    val input = readInputFile(Day3)

    val powerConsumption = calculatePowerConsumption(Diagnostic(input))
    val lifeSupport = calculateLifeSupport(Diagnostic(input))

    println("Day 3")
    println("Power consumption : $powerConsumption")
    println("Life support : $lifeSupport")
}

object Day3