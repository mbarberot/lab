package code.advent.edition2021.day3.reader.life

import code.advent.edition2021.day3.reader.BitString

data class LifeSupportRates(
    val oxygenGeneratorRate: BitString,
    val co2ScrubberRate: BitString
)