package code.advent.edition2021.day3

import code.advent.edition2021.day3.diagnostic.Diagnostic
import code.advent.edition2021.day3.reader.life.LifeSupportReader

fun calculateLifeSupport(diagnostic: Diagnostic): Int {
    val (oxygenGeneratorRate, co2ScrubberRate) = LifeSupportReader().read(diagnostic)

    return oxygenGeneratorRate.getAsDecimal() * co2ScrubberRate.getAsDecimal()
}