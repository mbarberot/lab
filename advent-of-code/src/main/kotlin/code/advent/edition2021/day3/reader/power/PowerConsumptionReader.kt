package code.advent.edition2021.day3.reader.power

import code.advent.edition2021.day3.diagnostic.Diagnostic
import code.advent.edition2021.day3.reader.BitStringBuilder

class PowerConsumptionReader {

    fun read(diagnostic: Diagnostic): PowerConsumptionRates {
        val gammaRate = BitStringBuilder()
        val epsilonRate = BitStringBuilder()

        diagnostic.columns.forEachIndexed { _, column ->
            gammaRate.append(column.mostCommonBit)
            epsilonRate.append(column.leastCommonBit)
        }

        return PowerConsumptionRates(
            gammaRate.build(),
            epsilonRate.build()
        )
    }
}