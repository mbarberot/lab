package code.advent.edition2021.day3.diagnostic

class Diagnostic(
    val rows: List<String>
) {
    val columns: List<Column> = reverseRows(rows)
}

private fun reverseRows(rows: List<String>): List<Column> {
    val columns = mutableListOf<String>()
    rows.forEachIndexed { rowIndex, row ->

        if (rowIndex == 0) {
            repeat(row.length) {
                columns.add("")
            }
        }

        row.forEachIndexed { columnIndex, char ->
            columns[columnIndex] = columns[columnIndex] + char
        }
    }
    return columns.map { Column(it) }
}