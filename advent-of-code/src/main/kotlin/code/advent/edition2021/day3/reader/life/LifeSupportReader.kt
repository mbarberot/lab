package code.advent.edition2021.day3.reader.life

import code.advent.edition2021.day3.diagnostic.Column
import code.advent.edition2021.day3.diagnostic.Diagnostic
import code.advent.edition2021.day3.reader.BitString

class LifeSupportReader {

    fun read(diagnostic: Diagnostic): LifeSupportRates {

        val oxygenGeneratorRating = findRatingBy(diagnostic) { column -> column.mostCommonBit }
        val co2ScrubberRating = findRatingBy(diagnostic) { column -> column.leastCommonBit }

        return LifeSupportRates(
            BitString(oxygenGeneratorRating),
            BitString(co2ScrubberRating),
        )
    }

    private fun findRatingBy(diagnostic: Diagnostic, columnIndex: Int = 0, filter: (column: Column) -> Char): String {

        if(columnIndex >= diagnostic.columns.size) {
            throw Error("OUT OF BOUND IN FIND")
        }

        val bitToFilterOn = filter.invoke(diagnostic.columns[columnIndex])
        val filteredRows = diagnostic.rows
            .filter { it[columnIndex] == bitToFilterOn }

        if(filteredRows.size == 1) {
            return filteredRows.first()
        }

        return findRatingBy(Diagnostic(filteredRows), columnIndex + 1, filter)
    }
}