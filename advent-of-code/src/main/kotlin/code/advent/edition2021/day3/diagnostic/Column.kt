package code.advent.edition2021.day3.diagnostic

data class Column(
    val bits: String
) {
    val zeroes = bits.count { it == '0' }
    val ones = bits.count { it == '1' }
    val mostCommonBit = if (zeroes > ones) '0' else '1'
    val leastCommonBit = if (zeroes > ones) '1' else '0'
}