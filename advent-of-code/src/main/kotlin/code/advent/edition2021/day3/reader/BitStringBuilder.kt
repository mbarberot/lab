package code.advent.edition2021.day3.reader

class BitStringBuilder {
    var builder = StringBuilder()

    fun append(bit: Char) {
        builder.append(bit)
    }

    fun build() = BitString(builder.toString())
}

