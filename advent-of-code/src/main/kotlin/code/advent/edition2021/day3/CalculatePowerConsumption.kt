package code.advent.edition2021.day3

import code.advent.edition2021.day3.diagnostic.Diagnostic
import code.advent.edition2021.day3.reader.power.PowerConsumptionReader

fun calculatePowerConsumption(diagnostic: Diagnostic): Int {
    val (gammaRate, epsilonRate) = PowerConsumptionReader().read(diagnostic)

    return gammaRate.getAsDecimal() * epsilonRate.getAsDecimal()
}