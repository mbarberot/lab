package code.advent.edition2021.day3.reader

data class BitString(
    val bits: String
) {
    fun getAsDecimal() = bits.toInt(2)
}