package code.advent.edition2021.day3.reader.power

import code.advent.edition2021.day3.reader.BitString

data class PowerConsumptionRates(
    val gammaRate: BitString,
    val epsilonRate: BitString,
)