package code.advent.edition2021.day1

class IncreaseCounter {
    fun countMeasurement(measurements: List<Int>): Int {
        var counter = 0
        var lastMeasurement: Int? = null

        for (measurement in measurements) {
            if (lastMeasurement != null) {
                if(measurement > lastMeasurement) {
                    counter++
                }
            }
            lastMeasurement = measurement
        }

        return counter
    }

}
