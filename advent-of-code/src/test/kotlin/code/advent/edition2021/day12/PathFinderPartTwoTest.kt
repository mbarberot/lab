package code.advent.edition2021.day12

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.collections.shouldHaveSize

class PathFinderPartTwoTest : StringSpec({

    "find paths in caves"() {
        // Arrange
        val caves = parseInput(
            """
            start-A
            start-b
            A-c
            A-b
            b-d
            A-end
            b-end
        """.trimIndent()
        )

        // Act
        val paths: List<String> = findPaths(caves)

        // Assert
        paths shouldHaveSize 36
    }

    "should return only one path"() {
        // Arrange
        val caves = parseInput(
            """
            start-end
        """.trimIndent()
        )

        // Act
        val paths = findPaths(caves)

        // Assert
        paths shouldContainExactlyInAnyOrder listOf("start,end")
    }

    "should explore different paths"() {
        // Arrange
        val caves = parseInput(
            """
            start-A
            A-end
            start-b
            b-end
            b-A
        """.trimIndent()
        )

        // Act
        val paths = findPaths(caves)

        // Assert
        paths shouldContainExactlyInAnyOrder listOf(
            "start,A,end",
            "start,A,b,end",
            "start,A,b,A,end",
            "start,A,b,A,b,end",
            "start,A,b,A,b,A,end",
            "start,b,end",
            "start,b,A,end",
            "start,b,A,b,end",
            "start,b,A,b,A,end",
        )
    }

    "should explore dead ends from a big cave"() {
        // Arrange
        val caves = parseInput(
            """
            start-A
            A-end
            b-A
        """.trimIndent()
        )

        // Act
        val paths = findPaths(caves)

        // Assert
        paths shouldContainExactlyInAnyOrder listOf(
            "start,A,end",
            "start,A,b,A,end",
            "start,A,b,A,b,A,end",
        )
    }

})

private fun parseInput(input: String) = input.split("\n")
private fun findPaths(caves: List<String>) = PathFinder(caves, version = 2).findPaths().toStringList()

