package code.advent.edition2021.day3

import code.advent.edition2021.day3.diagnostic.Diagnostic
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

private fun parseInput(input: String) = input.split("\n")

class LifeSupportTest : StringSpec({
    "calculate life support for simple input"() {
        // Arrange
        val input = parseInput("""
            00100
            11110
            10110
            10111
            10101
            01111
            00111
            11100
            10000
            11001
            00010
            01010
            """.trimIndent())

        // Act
        val result = calculateLifeSupport(Diagnostic(input))

        // Assert
        result shouldBe 230
    }

    "calculate life support : basic case"() {
        // Ararnge
        val input = parseInput("""
            00100
            11001
            11011
        """.trimIndent())

        // Act
        val result = calculateLifeSupport(Diagnostic(input))

        val expectedOxygenGeneratorRate = "11011".toInt(2) // 27
        val expectedC02ScrubberRate = "00100".toInt(2) // 4

        // Assert
        result shouldBe expectedOxygenGeneratorRate * expectedC02ScrubberRate
        result shouldBe 108
    }
})

