package code.advent.edition2021.day3

import code.advent.edition2021.day3.diagnostic.Diagnostic
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

private fun parseInput(input: String) = input.split("\n")

class PowerConsumptionTest : StringSpec({
    "calculate power consumption for simple input"() {
        // Arrange
        val input = parseInput("""
            00100
            11110
            10110
            10111
            10101
            01111
            00111
            11100
            10000
            11001
            00010
            01010
            """.trimIndent())

        // Act
        val result = calculatePowerConsumption(Diagnostic(input))

        // Assert
        result shouldBe 198
    }

    "calculate power consumption: basic case"() {
        // Ararnge
        val input = parseInput("""
            00100
            11011
            11011
        """.trimIndent())

        // Act
        val result = calculatePowerConsumption(Diagnostic(input))

        val expectedGamma = "11011".toInt(2) // 27
        val expectedEpsilon = "00100".toInt(2) // 4

        // Assert
        result shouldBe expectedGamma * expectedEpsilon
        result shouldBe 108
    }
})

