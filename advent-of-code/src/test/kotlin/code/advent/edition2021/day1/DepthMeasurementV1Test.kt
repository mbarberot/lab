package code.advent.edition2021.day1

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class DepthMeasurementV1Test: FunSpec() {
    init {
        test("one increase") {
            // given
            val measurements = listOf(
                199,
                200
            )

            // when
            val increaseCount = IncreaseCounter().countMeasurement(measurements)

            // then
            increaseCount shouldBe 1
        }

        test("no increase") {
            // given
            val measurements = listOf(199, 198)

            // when
            val increaseCount = IncreaseCounter().countMeasurement(measurements)

            // then
            increaseCount shouldBe 0
        }

        test("several increases") {
            // given
            val measurements = listOf(199, 200, 208, 210)

            // when
            val increaseCount = IncreaseCounter().countMeasurement(measurements)

            // then
            increaseCount shouldBe 3
        }

        test("variable measures") {
            // given
            val measurements = listOf(199, 200, 208, 210, 200, 207)

            // when
            val increaseCount = IncreaseCounter().countMeasurement(measurements)

            // then
            increaseCount shouldBe 4
        }
    }
}