package code.advent.edition2020.day3

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class TravelByTobogganV1Test : FunSpec() {
    init {
        test("no tree during travel") {
            val map = listOf(
                "..##",
                "#..."
            )

            travel(map) shouldBe 0
        }

        test("one tree during travel") {
            val map = listOf(
                "..##",
                "#..#"
            )

            travel(map) shouldBe 1
        }

        test("travel continues until bottom reached") {
            val map = listOf(
                "..##...",
                "#...#..",
                ".#....#",
            )

            travel(map) shouldBe 1
        }

        test("map extends to the left following the same pattern") {
            val map = listOf(
                "..##.......",
                "#...#...#..",
                ".#....#..#.",
                "..#.#...#.#",
                ".#...##..#.",
                "..#.##.....",
                ".#.#.#....#",
                ".#........#",
                "#.##...#...",
                "#...##....#",
                ".#..#...#.#"
            )

            travel(map) shouldBe 7
        }
    }

    fun travel(map: List<String>): Int {
        val treeCounter = TreeCounter()
        TravelByToboggan(map).travel(treeCounter)
        return treeCounter.getValue()
    }
}
