package code.advent.edition2020.day4

import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class Day4Test : FunSpec() {

    init {

        test("_valid") {
            val input = """
                ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
                byr:1937 iyr:2017 cid:147 hgt:183cm
                """.trimIndent()

            isValid(input) shouldBe true
        }

        test("_invalid") {
            val input = """
                iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
                hcl:#cfa07d byr:1929
            """.trimIndent()

            isValid(input) shouldBe false
        }

        test("_valid_north_pole") {
            val input = """
                hcl:#ae17e1 iyr:2013
                eyr:2024
                ecl:brn pid:760753108 byr:1931
                hgt:179cm
            """.trimIndent()

            isValid(input) shouldBe true
        }

        test("__invalid") {
            val input = """
                hcl:#cfa07d eyr:2025 pid:166559648
                iyr:2011 ecl:brn hgt:59in
            """.trimIndent()

            isValid(input) shouldBe false
        }
    }

    fun isValid(input: String): Boolean {


        val list = input.replace("\n", " ").split(" ")

        return when (list.size) {
            8 -> true
            7 -> !hasCID(list)
            else -> false
        }
    }

    private fun hasCID(list: List<String>): Boolean = list.any { field -> field.startsWith("cid:") }

}