package code.advent.edition2020.day2

import code.advent.edition2020.day2.rules.RuleExtractorV1
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class CheckPasswordV1Test : FunSpec() {
    init {
        test("valid password") {
            checkPassword("1-3 a: abcde") shouldBe Result.VALID
        }

        test("invalid password") {
            checkPassword("1-3 b: cdefg") shouldBe Result.INVALID
        }

        test("another valid password") {
            checkPassword("2-9 c: ccccccccc") shouldBe Result.VALID
        }
    }

    fun checkPassword(input: String): Result = PasswordChecker(RuleExtractorV1()).checkPassword(input)
}