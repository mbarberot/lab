package code.advent.edition2020.day2

import code.advent.edition2020.day2.rules.RuleExtractorV2
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe

class CheckPasswordV2Test : FunSpec() {
    init {
        test("valid password") {
            checkPassword("1-3 a: abcde") shouldBe Result.VALID
        }

        test("invalid password") {
            checkPassword("1-3 b: cdefg") shouldBe Result.INVALID
        }

        test("another invalid password") {
            checkPassword("2-9 c: ccccccccc") shouldBe Result.INVALID
        }
    }

    fun checkPassword(input: String): Result = PasswordChecker(RuleExtractorV2()).checkPassword(input)
}





