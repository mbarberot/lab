package code.advent.edition2022.day01

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class ParseCaloriesTests() : StringSpec({

    "parse simple calories list" {
        // arrange
        val elfCalories = listOf(
            "1000",
            "",
            "2000"
        )

        // act
        val caloriesByElf = parseCaloriesList(elfCalories)

        // assert
        caloriesByElf.size shouldBe 2
        caloriesByElf[0].total shouldBe 1000
        caloriesByElf[1].total shouldBe 2000
    }

    "parse calories list with multiple item by elf" {
        // arrange
        val elfCalories = listOf(
            "1000",
            "2000",
            "",
            "2000",
            "2000"
        )

        // act
        val caloriesByElf = parseCaloriesList(elfCalories)

        // assert
        caloriesByElf.size shouldBe 2
        caloriesByElf[0].total shouldBe 3000
        caloriesByElf[1].total shouldBe 4000
    }
})