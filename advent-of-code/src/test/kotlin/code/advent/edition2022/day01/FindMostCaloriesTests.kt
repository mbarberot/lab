package code.advent.edition2022.day01

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class FindMostCaloriesTests() : StringSpec({

    "find most calories (given example)" {
        // arrange
        val elfCalories = listOf(
            "1000",
            "2000",
            "3000",
            "",
            "4000",
            "",
            "5000",
            "6000",
            "",
            "7000",
            "8000",
            "9000",
            "",
            "10000"
        )

        // act
        val mostCalories = findMostCalories(
            parseCaloriesList(elfCalories)
        )

        // assert
        mostCalories shouldBe 24_000
    }

    "find most calories (simpler example)" {
        // arrange
        val elfCalories = listOf(
            "1000",
            "",
            "2000",
        )

        // act
        val mostCalories = findMostCalories(
            parseCaloriesList(elfCalories)
        )

        // assert
        mostCalories shouldBe 2000
    }


})

