package code.advent.edition2022.day01

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class FindTop3MostCaloriesTests() : StringSpec({

    "find top 3 most calories (given example)" {
        // arrange
        val elfCalories = listOf(
            "1000",
            "2000",
            "3000",
            "",
            "4000",
            "",
            "5000",
            "6000",
            "",
            "7000",
            "8000",
            "9000",
            "",
            "10000"
        )

        // act
        val mostCalories = findTop3MostCalories(
            parseCaloriesList(elfCalories)
        )

        // assert
        mostCalories shouldBe 45_000
    }

    "find most calories (simpler example)" {
        // arrange
        val elfCalories = listOf(
            "1000",
            "",
            "2000",
            "",
            "3000"
        )

        // act
        val mostCalories = findTop3MostCalories(
            parseCaloriesList(elfCalories)
        )

        // assert
        mostCalories shouldBe 6000
    }
})



