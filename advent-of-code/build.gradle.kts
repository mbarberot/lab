plugins {
    kotlin("jvm") version "1.6.10"
    id("com.adarshr.test-logger") version "3.0.0"
}

group = "code.advent"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

tasks {
    test {
        useJUnitPlatform()
        testLogging {
            showStandardStreams = true
        }
    }
}

configure<com.adarshr.gradle.testlogger.TestLoggerExtension> {
    theme = com.adarshr.gradle.testlogger.theme.ThemeType.MOCHA
}

dependencies {
    val kotestVersion = "4.6.2"

    implementation(kotlin("stdlib"))
    testImplementation("io.kotest:kotest-runner-junit5:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core:$kotestVersion")
}

